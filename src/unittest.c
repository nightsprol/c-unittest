#include "unittest.h"

/*
  Basic unit test framework
*/

// Program flags
int VERBOSITY, FAIL_FAST, LIST_TESTS, RUN_TEST, ALWAYS_SUCCEED;

// Unit test globals
char *ut_suite;
int ut_count, ut_total, ut_passed, ut_failed, ut_crashed;

/**
 * Wrapper for a unit test
 *  
 * @param test      The test function to wrap 
 * @param test_name The name of the test
 * @param init      Initialize function, called before the test if non-null
 * @param fini      Finalizer function, called after the test if non-null
 * @param exit_code The expected exit code of the test (defaults to 0)
 */
void
unit_test(ut_f test, char *test_name, ut_f init, ut_f fini, int exit_code)
{
  int exit_status;

  // Just print the name of the test if LIST_TESTS was specified
  if(LIST_TESTS) {
    printf("\t%d. %s\n", ut_count, test_name);
    ut_count++;
    return;
  }

  // Don't run this test if a certain one was specified
  if(RUN_TEST >= 0) {
    if(RUN_TEST != ut_count) {
      ut_count++;
      return;
    }

    if(VERBOSITY >= V_DEFAULT)
      printf(HDR_STR "Running " KBLU "%s" KNRM " from " KBWN "%s" KNRM ":\n",
             test_name, ut_suite);

  } else if(VERBOSITY >= V_DEFAULT) {
    printf(RUN_STR, ut_suite, test_name);
  }

  ut_total++;
  
  // Call the unit test
  if(!fork()) {
    if(init) init();
    test();
    if(fini) fini();
    exit(UT_SUCCESS);
  }

  // Reap the unit test and get the return code
  wait(&exit_status);

  if(WIFEXITED(exit_status))
    exit_status = (int)WEXITSTATUS(exit_status);
  
  // Print out success or failure
  if(exit_status != exit_code) {
    if(VERBOSITY >= V_DEFAULT) {
      if(exit_status == UT_FAILURE)
        printf(FAIL_STR "\n", ut_suite, test_name);
      else
        printf(CRASH_STR, ut_suite, test_name, exit_status);
    }
    
    ut_failed++;

    if(exit_status != UT_FAILURE)
      ut_crashed++;
    
    // Exit on test failure if specified
    if(FAIL_FAST)
      fini_tests();
  } else {
    if(VERBOSITY >= V_DEFAULT)
      printf(PASS_STR, ut_suite, test_name);
    ut_passed++;
  }

  ut_count++;
}

// Parse command line arguments
void
parse_args(int argc, char *argv[])
{
  int i = 1;
  char *arg;
  
  for(; i < argc; i++) {
    arg = argv[i];
    
    if(!strcmp(arg, "-h") || !strcmp(arg, "--help")) {
      USAGE(argv[0], UT_SUCCESS);
    } else if(!strcmp(arg, "-q") || !strcmp(arg, "--quiet")) {
      VERBOSITY = V_QUIET;
    } else if(!strcmp(arg, "-f") || !strcmp(arg, "--fail-fast")) {
      FAIL_FAST = 1;
    } else if(!strcmp(arg, "-l") || !strcmp(arg, "--list")) {
      LIST_TESTS = 1;
    } else if(!strcmp(arg, "-r") || !strcmp(arg, "--run")) {
      if(i == argc - 1)
        USAGE(argv[0], UT_FAILURE);
      RUN_TEST = atoi(argv[++i]);
    } else if(!strcmp(arg, "--always-succeed")) {
      ALWAYS_SUCCEED = 1;
    } else {
      USAGE(argv[0], UT_FAILURE);
    }
  }
}

// Initialize the unit tests
void
init_tests(int argc, char *argv[])
{
  // Set flag defaults
  VERBOSITY      = V_DEFAULT;
  FAIL_FAST      = 0;
  ALWAYS_SUCCEED = 0;
  LIST_TESTS     = 0;
  RUN_TEST       = -1;
  
  // Parse the command line args
  parse_args(argc, argv);

  // Replace the '.' with '\0' in the suite name
  /*
  for(char *p = ut_suite; *p; p++) {
    if(*p == '.') {
      *p = 0;
      break;
    }
  }
  */
  
  if(!LIST_TESTS && RUN_TEST < 0 && VERBOSITY >= V_DEFAULT)
    printf(HDR_STR "Running tests from " KBWN "%s" KNRM ":\n", ut_suite);

  // Initialize all test statistics
  ut_count   = 0;
  ut_total   = 0;
  ut_passed  = 0;
  ut_failed  = 0;
  ut_crashed = 0;
}

// Print out tests synthesis and exit
void
fini_tests(void)
{
  if(!LIST_TESTS && VERBOSITY >= V_DEFAULT)
    printf(FINAL_STR, ut_total, ut_passed,
           ut_failed ? KRED : KNRM, ut_failed,
           ut_crashed ? KRED : KNRM, ut_crashed);

  int exit_code = ut_failed ? UT_FAILURE : UT_SUCCESS;
  exit(ALWAYS_SUCCEED ? UT_SUCCESS : exit_code);
}
