#ifndef UNITTEST_H
#define UNITTEST_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "colors.h"
#include "debug.h"
#include "named_params.h"

#define NL "\n"
#define STDERR 2

#define UT_SUCCESS 0
#define UT_FAILURE 255

// Globals
extern char *ut_suite;
extern int ut_total, ut_passed, ut_failed, ut_crashed;

// Flags
extern int VERBOSITY, FAIL_FAST, ALWAYS_SUCCEED;

// Program arg flag values
#define V_QUIET   0
#define V_DEFAULT 1

// Format strings
#define HDR_STR   "[" KBLU "====" KNRM "] "
#define INFO_STR  "[" KBLU "----" KNRM "] " BOLD "%s" KNRM " : " // suite
#define RUN_STR   "[" KBLU "RUN " KNRM "] " BOLD "%s" KNRM " : %s\n" // suite, test
#define PASS_STR  "[" KGRN "PASS" KNRM "] " BOLD "%s" KNRM " : %s\n" // suite, test
#define FAIL_STR  "[" KRED "FAIL" KNRM "] " BOLD "%s" KNRM " : %s"   // suite, test
#define CRASH_STR FAIL_STR " exited with " KRED "%d" KNRM "\n" // suite, test, exit status
#define FINAL_STR HDR_STR "Synthesis: " \
  "Tested: "  KBLU "%d" KNRM " | " \
  "Passing: " KGRN "%d" KNRM " | " \
  "Failed: %s%d" KNRM " | " \
  "Crashing: %s%d" KNRM "\n" // total, passed, color1, failed, color2, crashed

#define ASSERT_FAIL INFO_STR "[" KRED "%d" KNRM "] Assertion failed: " // suite, lineno, cond

// Assertion macro, prints msg and exits test on failure
#define assert(cond, msg, ...) \
  do { \
    if(!(cond)) { \
      if(VERBOSITY >= V_DEFAULT) { \
        if(*msg) { \
          printf(ASSERT_FAIL msg NL, ut_suite, __LINE__, ##__VA_ARGS__); \
        } else { \
          printf(ASSERT_FAIL STR(cond) NL, ut_suite, __LINE__); \
        } \
      exit(UT_FAILURE); \
      } \
    } \
  } while(0)

// Call this at the start of main()
#define start_tests(argc, argv) \
  do { \
    ut_suite = __FILE__; \
    init_tests(argc, argv); \
  } while(0);

// Call this at the end of main() after a call to start_tests()
#define end_tests() \
  do { \
    fini_tests(); \
  } while(0);

#define STR1(x) #x
#define STR(x)  STR1(x)

// Typedef'd function pointer for generic functions
typedef void (*ut_f)(void);

// Special case of named args for unit_test() that has 2 required arguments
#define UT_NAMED_ARGS(func, arg0, arg1, args, ...) ({ \
      ARGS_STRUCT args _args = {__VA_ARGS__}; \
      func(arg0, arg1, PASS_STRUCT args); \
    })

// Register a unit test with optional parameters
#define Test(test, ...) \
  UT_NAMED_ARGS(unit_test, &test, STR(test), \
                     (ut_f init, ut_f fini, int exit_code), __VA_ARGS__)

// Print program usage and exitstat(exit_code)
#define USAGE(prog, exit_code) \
  do { \
      printf("usage: %s OPTIONS\n" \
               "options:\n" \
               "\t-h or --help: prints this message\n" \
               "\t-q or --quiet: disables all logging\n" \
               "\t-f or --fail-fast: exit after the first failure\n" \
               "\t-l or --list: list all unit tests\n" \
               "\t-r N or --run N: run only the test corresponding to N\n" \
               "\t--always-succeed: always exit with 0\n", \
               prog); \
      exit(exit_code); \
  } while(0);

// Function prototypes
void unit_test(ut_f test, char *test_name, ut_f init, ut_f fini, int exit_code);
void init_tests(int, char**);
void fini_tests(void);

#endif /* End header guard */
