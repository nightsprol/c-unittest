#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include "colors.h"

#define NL "\n"

#ifdef DEBUG
#define debug(S, ...) \
  do { \
    printf(2, KMAG "DEBUG: %s:%s:%d " KNRM S NL, __FILE__, \
            __extension__ __FUNCTION__, __LINE__, ##__VA_ARGS__); \
  } while(0)
#else
#define debug(S, ...)
#endif

#endif /* HEADER GUARD */
