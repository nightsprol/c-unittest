#include "unittest.h"

/*
  Testing special functionality of the unit test library.
*/

// Structs
typedef struct test_t {
  uint value;
  char byte;
  void *pointer;
} test_t;

// Globals
test_t *test;

// Helpers
static void
test_init(void)
{
  test = (test_t*) malloc(sizeof(test_t));
  test->value = 15;
  test->byte = 'A';
  test->pointer = test;
}

static void
test_fini(void)
{
  free(test);
}

// Tests
void
test_assert_fail_no_args(void)
{
  assert(24 == 18, "");
}

void
test_assert_fail_args(void)
{
  assert(0, "expected assert to fail");
}

void
test_init_and_fini(void)
{
  assert(test, "");
  assert(test->value == 15, "test->value was %d, expected %d", test->value, 15);
  assert(test->byte == 'A', "test->byte was %c, expected %c", test->byte, 'A');
  assert(test->pointer == test, "");
}

void
test_none(void)
{
  assert(!test, "");
}

// Main Execution
int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(test_assert_fail_no_args, .exit_code=UT_FAILURE);
  Test(test_assert_fail_args, .exit_code=UT_FAILURE);
  Test(test_init_and_fini, .init=test_init, .fini=test_fini);
  Test(test_none);
  
  end_tests();
}
