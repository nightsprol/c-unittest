# C Unit Test Library

Eventually I'll come up with a better name.

# Downloading

To use this library, clone the repository and run `make` to compile.

```sh
$ git clone git@gitlab.com:nightsprol/c-unittest.git
$ cd c-unittest
$ make
```

The unit test library object (.o) file will be in the `build` directory.
You can add this in your project to use it for unit testing.

# Writing Unit Tests

An example unit test is included in the `tests` directory.
It demonstrates how to register unit tests, how to perform assertions,
and some of the more advanced test use cases.

All tests should use the following basic structure:

```c
#include "unittest.h"

/* Write tests and helper functions here */
void dummy(void) {
     assert(1 == 1, "%d did not equal %d", 1, 1);
}

int main(int argc, char *argv[]) {
    start_tests(argc, argv);

    /* Register tests here */
    Test(dummy);

    end_tests();
}
```

You can provide optional arguments per unit test when registering them.
Continuing with the above example, the format would be `Test(dummy, .exit_code=1)`.
The available options are listed below:

- `.init`
  - Specify an initializer function that will be called before running the test
- `.fini`
  - Specify a finalizer function that will be called after running the test
  - **Note**: This will not be called if the test fails
- `.exit_code`
  - Specify the expected exit code of the unit test

# Command Line Flags

There are several command line arguments that can be passed to any tests set up
using this library.
You can see the full usage by running compiled unit tests with `-h`.
Some of them are listed below along with their functionality:

- `-l or --list`
  - List out all the unit test names in order with their indices
  - This is especially useful for the flag `-r`
- `-r N or --run N`
  - Run only the test corresponding to the index N
  - This is useful when you have a large test suite or many slow tests
  - If you're only failing one particular test this allows you to run just that one

# Notes

This library was originally created for use in the **xv6 Operating System**.
For this reason, the library was written without use of threads or signals, since xv6
does not have these implemented in the basecode.
If you need, I can provide the original source code for that version of the library
but it may not be as well maintained.
It also requires the addition of two system calls, specifically some version of `exit()`
that passes an exit status and some version of `wait()` that acquires the exit status.
Again, I have the source code for these system calls if needed.

This library was inspired by and heavily based on the Criterion unit testing framework.
Most of the unit test output and even some of the structure is implemented to mirror
how that framework looks and works.
Some of the more recent changes have caused it to diverge as I've found some things were
not needed or could be more readable.

# ToDo

Here is an incomplete list of some of the features that need to be added, in no particular order:

- Exit tests using signals
- Catch crashed tests appropriately (worked in xv6)
- Catch crashing `init` and `fini` functions appropriately
- Use threads to speed up test run times
- Rename the project to something more interesting

# Contributing

You can contribute by cloning the master branch and creating your own separate development branch.

```sh
$ git clone git@gitlab.com:nightsprol/c-unittest.git
$ cd c-unittest
$ git checkout -b <new-branch>
```

# Authors

- Nate Wood